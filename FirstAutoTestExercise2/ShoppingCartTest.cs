﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace FirstAutoTestExercise2
{    
    [TestFixture]
    public class ShoppingCartTest
    {
        private IWebDriver driver;
        private WebDriverWait Wait;
        private IWebElement indexPage => driver.FindElement(By.Id("page"));
        private IWebElement searchInput => indexPage.FindElement(By.Name("search_query"));
        private IWebElement searchButton => indexPage.FindElement(By.Name("submit_search"));
        private IWebElement centerColumn => indexPage.FindElement(By.Id("center_column"));
        private IWebElement itemName => centerColumn.FindElement(By.CssSelector("a.product-name"));
        private IWebElement cartForm => centerColumn.FindElement(By.CssSelector("div.box-cart-bottom"));
        private IWebElement cartButton => cartForm.FindElement(By.Name("Submit"));
        private IWebElement cartModal => indexPage.FindElement(By.Id("layer_cart"));
        private IList<IWebElement> SearchedItemNames => centerColumn.FindElements(By.CssSelector("a.product-name"));

        private void WaitForResult(Func<IWebDriver, bool> driverElement)
        {
            Wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            Wait.Until(driverElement);

        }

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("http://automationpractice.com/index.php");
        }


        [Test]
        public void GivenAnExistingItemName_WhenSearching_ThenReturnItemCountGreaterThanZero()
        {
            searchInput.SendKeys("Dress");
            searchButton.Click();

            WaitForResult(driver => SearchedItemNames.Count > 0);

            Assert.IsTrue(SearchedItemNames.Count > 0);
        }

        [Test]
        public void GivenAnExistingItemName_WhenAddingTocart_ThenDisplayCartModal()
        {
            searchInput.SendKeys("Dress");
            searchButton.Click();

            itemName.Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            WaitForResult(driver => cartForm.Displayed);

            cartButton.Click();

            WaitForResult(driver => cartModal.Displayed);

            Assert.IsTrue(cartModal.Displayed);
        }

        [TearDown]
        public void TearDown()
        {
            driver.Dispose();
        }
    }
}
